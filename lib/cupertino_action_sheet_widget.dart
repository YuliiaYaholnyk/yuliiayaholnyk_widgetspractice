import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoActionSheetWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: TabBar(
          tabs: <Widget>[
            Tab(
              text: 'Cat',
            ),
            Tab(
              text: 'Dog',
            ),
            Tab(
              text: 'Rabbit',
            ),
          ],
        ),
        title: Text('Tab Bar'),
      ),
      body: TabBarView(
        children: <Widget>[
          Center(child: Image(
            image: NetworkImage('https://w1.pngwave.com/png/239/218/403/orange-cat-cartoon-small-to-mediumsized-cats-whiskers-head-snout-nose-png-clip-art.png'),
          ),),
          Center(child: Image(
            image: NetworkImage('https://img.favpng.com/1/0/12/cat-and-dog-cartoon-png-favpng-eEJ7ScvwiwcidkjzNYNS8yAGY.jpg'),
          ),),
          Center(child: Image(
            image: NetworkImage('https://st.depositphotos.com/1007989/1348/i/450/depositphotos_13488244-stock-photo-rabbit-front-view.jpg'),
          ),),
        ],
      ),
    );
  }
}
