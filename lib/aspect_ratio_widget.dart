import 'package:flutter/material.dart';

class AspectRatioWidget extends StatelessWidget {
  static const String routeName = 'detailPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text('Limited Box'),
      ),
      body: ListView.builder(
        itemCount: 100,
        itemBuilder: (BuildContext context, int i) => LimitedBox(
          maxHeight: 100,
          child: Container(
            color: Colors.yellow,
            margin: EdgeInsets.all(20),
          ),
        ),
      ),
    );
  }
}
