import 'package:flutter/material.dart';

class DissmissibleWidget extends StatefulWidget {
  @override
  _DissmissibleWidgetState createState() => _DissmissibleWidgetState();
}

class _DissmissibleWidgetState extends State<DissmissibleWidget> {
  final list = List.generate(5, (int index) => index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dissmissible widget'),
      ),
      body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context, int i) => Dismissible(
                onDismissed: (direction) {
                  setState(() {
                    list.removeAt(i);
                  });
                },
                child: Card(
                  color: Colors.green,
                  elevation: 5.0,
                  margin: EdgeInsets.all(10.0),
                  child: Text(list[i].toString()),
                ),
              )),
    );
  }
}
