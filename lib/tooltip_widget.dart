import 'package:flutter/material.dart';

class TooltipWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text('ToolTip'),
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(20),
          child: IconButton(
            icon: Icon(Icons.opacity),
            iconSize: 50.0,
            tooltip: 'Opacity',
            onPressed: () {},
          ),
        ),
      ),
    );
  }
}
