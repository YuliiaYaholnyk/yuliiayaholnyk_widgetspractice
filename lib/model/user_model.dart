import 'package:shared_preferences/shared_preferences.dart';

class UserModel {
  final String image;
  final String name;
  final String phone;
  final String email;

  UserModel({
    this.name,
    this.image,
    this.phone,
    this.email,
  });

  Future<void> saveToPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('image', image);
    prefs.setString('name', name);
    prefs.setString('phone', phone);
    prefs.setString('email', email);
  }

  Future<UserModel> getFromPrefs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    UserModel user = copyWith(name: prefs.getString('name'));
    user = copyWith(email: prefs.getString('email'));
    user = copyWith(image: prefs.getString('image'));
    user = copyWith(phone: prefs.getString('phone'));
    return user;
  }

  UserModel copyWith({
    String name,
    String image,
    String phone,
    String email,
  }) {
    return UserModel(
      phone: phone ?? this.phone,
      email: email ?? this.email,
      image: image ?? this.image,
      name: name ?? this.name,
    );
  }
}
