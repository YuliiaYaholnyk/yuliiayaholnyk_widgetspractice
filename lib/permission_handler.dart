import 'dart:io';

import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart';

class PermissionPage extends StatefulWidget {
  @override
  _PermissionPageState createState() => _PermissionPageState();
}

class _PermissionPageState extends State<PermissionPage> {
  Position position;

  @override
  void initState() {
    Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high).then((value) {
      position = value;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Package practice #2'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                  children: Permission.values
                      .where((Permission permission) {
                        if (Platform.isIOS) {
                          return permission != Permission.unknown &&
                              permission != Permission.sms &&
                              //permission != Permission.storage &&
                              permission != Permission.ignoreBatteryOptimizations &&
                              permission != Permission.accessMediaLocation;
                        } else {
                          return permission != Permission.unknown &&
                              permission != Permission.mediaLibrary &&
                              permission != Permission.photos &&
                              permission != Permission.reminders;
                        }
                      })
                      .map((permission) => PermissionWidget(permission))
                      .toList()),
            ),
            ClipRRect(
              borderRadius: BorderRadius.only(
                  topLeft:Radius.circular(10.0),
              topRight: Radius.circular(10.0),
              ),
              child: Container(
                width: double.infinity,
                alignment: Alignment.center,
                height: 100.0,
                color: Colors.blue,
                child: Text(
                  'Geolocation:\n${position.toString()}',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class PermissionWidget extends StatefulWidget {
  /// Constructs a [PermissionWidget] for the supplied [Permission].
  const PermissionWidget(this._permission);

  final Permission _permission;

  @override
  _PermissionState createState() => _PermissionState(_permission);
}

class _PermissionState extends State<PermissionWidget> {
  _PermissionState(this._permission);

  final Permission _permission;
  PermissionStatus _permissionStatus = PermissionStatus.undetermined;

  @override
  void initState() {
    super.initState();

    _listenForPermissionStatus();
  }

  void _listenForPermissionStatus() async {
    final status = await _permission.status;
    setState(() => _permissionStatus = status);
  }

  Color getPermissionColor() {
    switch (_permissionStatus) {
      case PermissionStatus.denied:
        return Colors.red;
      case PermissionStatus.granted:
        return Colors.green;
      default:
        return Colors.grey;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(_permission.toString()),
      subtitle: Text(
        _permissionStatus.toString(),
        style: TextStyle(color: getPermissionColor()),
      ),
      trailing: IconButton(
          icon: const Icon(Icons.info),
          onPressed: () {
            checkServiceStatus(context, _permission);
          }),
      onTap: () {
        requestPermission(_permission);
      },
    );
  }

  void checkServiceStatus(BuildContext context, Permission permission) async {
    Scaffold.of(context).showSnackBar(SnackBar(
      content: Text((await permission.status).toString()),
    ));
  }

  Future<void> requestPermission(Permission permission) async {
    final status = await permission.request();

    setState(() {
      print(status);
      _permissionStatus = status;
      print(_permissionStatus);
    });
  }
}
