import 'package:flutter/material.dart';
import 'dart:math';

class CirleCanvas extends StatefulWidget {
  @override
  _CirleCanvasState createState() => _CirleCanvasState();
}

class _CirleCanvasState extends State<CirleCanvas> with TickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  Animation<double> sizeAnimation;
  AnimationController sizeController;
  Tween<double> _rotationTween = Tween(begin: -pi, end: pi);
  Tween<double> _sizeTween = Tween(begin: 0.0, end: 300.0);

  @override
  void dispose() {
    controller.dispose();
    sizeController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 4),
    );
    sizeController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 4),
    );
    sizeAnimation = _sizeTween.animate(sizeController)..addListener(() {
      setState(() {

      });
    })..addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        sizeController.repeat(reverse: true);
      } else if (status == AnimationStatus.dismissed) {
        sizeController.forward();
      }
    });
    animation = _rotationTween.animate(controller)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });
    sizeController.forward();
    controller.forward();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Custom Paint'),
      ),
      body: Center(
        child: Transform.rotate(
          angle: animation.value,
          child: Container(
            height: sizeAnimation.value,
            width: double.infinity,
            child: CustomPaint(
              painter: CirclePaint(),
            ),
          ),
        ),
      ),
    );
  }
}

class CirclePaint extends CustomPainter {
  final double animationValue;

  CirclePaint({this.animationValue});

  @override
  void paint(Canvas canvas, Size size) {
    Paint line = new Paint()
      ..color = Colors.black87
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line2 = new Paint()
      ..color = Colors.green
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line3 = new Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line4 = new Paint()
      ..color = Colors.yellow
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint line5 = new Paint()
      ..color = Colors.purple
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 5;
    Paint complete = new Paint()
      ..color = Colors.red
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = 10;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, line);
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), -pi / 2.5, 1, false, line2);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 6, 1, false, line3);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / -7, 1.1, false, line4);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / -2, 1.6, false, line5);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 1.5, 1.1, false, line);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius), -pi / 1, 1.1, false, complete);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
