import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:widgetsPractice/color_filtred_widget.dart';
import 'package:widgetsPractice/place_holder.dart';


class PageViewWidget extends StatefulWidget {
  @override
  _PageViewWidgetState createState() => _PageViewWidgetState();
}

class _PageViewWidgetState extends State<PageViewWidget> {
   PageController controller;

  @override
  void initState() {
    controller = PageController(
      initialPage: 1,
    );
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PageView'),
      ),
      body: PageView(
        controller: controller,
        scrollDirection: Axis.vertical,
        children: <Widget>[
          ColorFiltredWidget(),
          ColorFiltredWidget(),
          ColorFiltredWidget(),
        ],
      ),
    );
  }
}
