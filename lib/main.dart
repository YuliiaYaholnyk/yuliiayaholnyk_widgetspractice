import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:widgetsPractice/clip_r_rect_widget.dart';
import 'package:widgetsPractice/permission_handler.dart';
import 'package:widgetsPractice/shared_preferences.dart';
import 'package:widgetsPractice/slider_widget.dart';
import 'package:widgetsPractice/snack_bar.dart';
import 'package:widgetsPractice/transform_widget.dart';

import 'Spacer.dart';
import 'about_dialog.dart';
import 'alert_dialog_widget.dart';
import 'check_box_list_tile.dart';
import 'circle_canvas.dart';
import 'circular_progress_indicator.dart';
import 'color_filtred_widget.dart';
import 'cupertino_action_sheet_widget.dart';
import 'data_table_widget.dart';
import 'draggable_scrollable_sheet.dart';
import 'igmore_pointer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Slidable Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: PermissionPage(),
      ),
    );
  }
}

