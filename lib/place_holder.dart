import 'package:flutter/material.dart';

class PlaceHolderWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FractionallySizedBox'),
      ),
      body: Center(
        child: FractionallySizedBox(
          alignment: Alignment.center,
          heightFactor: 0.5,
          widthFactor: 0.5,
          child: Placeholder(
            color: Colors.green,
            fallbackHeight: 200,
          ),
        ),
      ),
    );
  }
}
