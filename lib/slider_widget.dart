import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SliderWidget extends StatefulWidget {
  @override
  _SliderWidgetState createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  double val1 = 0;
  double val2 = 0;
  var val3 = RangeValues(0.0,2.0);
  double val4 =0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Slider(
            value: val1,
            onChanged: (newVal) {
              setState(() {
                val1 = newVal;
              });
            },
            min: 0,
            max: 100,
            label: val1.toString(),
          ),
          CupertinoSlider(
            value: val4,
            onChanged: (newVal) {
              setState(() {
                val4 = newVal;
              });
            },
            min: 0,
            max: 100,
           activeColor: Colors.red,
            thumbColor: Colors.blue,
          ),
          Slider(
            value: val2,
            onChanged: (newVal) {
              setState(() {
                val2 = newVal;
              });
            },
            min: 0,
            max: 100,
            divisions: 6,
            label: val2.toStringAsFixed(2),
          ),
          RangeSlider(
            values: val3,
            onChanged: (newVal) {
              setState(() {
                val3 = newVal;
              });
            },
            min: 0.0,
            max: 2.0,
divisions: 100,
            labels: RangeLabels(val3.start.toString(), val3.end.toString()),
          ),
        ],
      ),
    );
  }
}
