import 'package:flutter/material.dart';

class ListViewWithListTileWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ListView'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),

        children: <Widget>[
          ListTile(
            leading: Icon(Icons.ac_unit, color: Colors.blue,),
            title: Text('I`m listTile title'),
            contentPadding: EdgeInsets.all(10.0),
            subtitle: Text('I`m subtitle'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.ac_unit, color: Colors.blue,),
            title: Text('I`m listTile title'),
            contentPadding: EdgeInsets.all(10.0),
            subtitle: Text('I`m subtitle'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.ac_unit, color: Colors.blue,),
            title: Text('I`m listTile title'),
            contentPadding: EdgeInsets.all(10.0),
            subtitle: Text('I`m subtitle'),
            onTap: () {},
          ),
          ListTile(
            leading: Icon(Icons.ac_unit, color: Colors.blue,),
            title: Text('I`m listTile title'),
            contentPadding: EdgeInsets.all(10.0),
            subtitle: Text('I`m subtitle'),
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
