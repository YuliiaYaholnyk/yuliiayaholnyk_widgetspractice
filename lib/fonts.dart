
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';

class Fonts {


  static TextStyle version({double size = 12.0}) {
    return GoogleFonts.roboto(
      color: WHITE,
      fontSize: size,
    );
  }
  static TextStyle splashScreen({double size = 60.0}){
    return GoogleFonts.merienda(
      fontWeight: FontWeight.bold,
      color: MARI_GOLD,
      fontSize: size,
    );
  }
  static TextStyle roboto_regular({double size = 60.0, Color color = BLACK}){
    return GoogleFonts.roboto(
      color: color,
      fontSize: size,
    );
  }


  static TextStyle loadingLayout ({double size = 16.0}){
    return GoogleFonts.roboto(
      color: WHITE,
      fontSize: size,
    );
  }
  static TextStyle globalButton ({double size = 24.0}){
    return GoogleFonts.roboto(
      fontWeight: FontWeight.w500,
      color: WHITE,
      fontSize: size,
    );
  }
}

const Color BG_COLOR = const Color(0xFF222222);
const Color PRIMARY_COLOR = const Color(0xFF222222);
const Color PRIMARY_WHITE = const Color(0xFFEEF0F5);

const Color WHITE = const Color(0xFFFFFFFF);
const Color BLACK = const Color(0xFF000000);
const Color BLACK_SHADOW = const Color(0x1A000000);
const Color SELECTED_ITEM_COLOR = const Color(0xFFFF9D56);
const Color NOT_SELECTED_ITEM_COLOR = const Color(0xFF808080);
const Color BUTTON_SHADOW = const Color(0xFFc79300);
const Color BLACK_WITH_OPACITY =const Color(0x80000000);


const Color WHEAT = const Color(0xFFffde81);
const Color MARI_GOLD = const Color(0xFFFFBD00);

const List<BoxShadow> PRIMARY_WHITE_SHADOW = const [
  const BoxShadow(
    color: const Color(0x33FFFFFF),
    offset: Offset(2, 2),
    blurRadius: 4,
  ),
  const BoxShadow(
    color: const Color(0x1AFFFFFF),
    offset: Offset(0, 4),
    blurRadius: 2,
  ),
];
