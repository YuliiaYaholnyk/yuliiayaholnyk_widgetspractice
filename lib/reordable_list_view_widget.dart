import 'package:flutter/material.dart';


class ReordableListView extends StatefulWidget {
  @override
  _ReordableListViewState createState() => _ReordableListViewState();
}

class _ReordableListViewState extends State<ReordableListView> {
  final list = List.generate(10, (index) => index);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FractionallySizedBox'),
      ),
      body: Container(
        width: 200.0,
        height: 100.0,
        color: Color.fromARGB(255, 235, 237, 237),
        child: FractionallySizedBox(
          widthFactor: 0.5,
          heightFactor: 0.25,
          alignment: Alignment.bottomRight,
          child:  RaisedButton(
            child: Text('Click'),
            color: Colors.green,
            textColor: Colors.white,
            onPressed: () {
            },
          ),
        ),
      ),
    );
  }
}
