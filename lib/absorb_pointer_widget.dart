import 'package:flutter/material.dart';



class AbsorbPointerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Absorb Pointer'),),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Container(color: Colors.red),
          AbsorbPointer(
              absorbing: false,
              child: Container(width: 42, height: 42, color: Colors.blue)),
        ],
      ),
    );
  }
}
