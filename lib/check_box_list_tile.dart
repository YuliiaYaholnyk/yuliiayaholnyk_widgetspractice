import 'package:flutter/material.dart';

class CheckBoxListTileWidget extends StatefulWidget {
  @override
  _CheckBoxListTileWidgetState createState() => _CheckBoxListTileWidgetState();
}

class _CheckBoxListTileWidgetState extends State<CheckBoxListTileWidget> {
  bool _isChecked = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('CheckBoxListTileWidget'),
      ),
      body: Container(
        margin: EdgeInsets.all(20.0),
        decoration: BoxDecoration(border: Border.all(color: Colors.teal)),
        child: CheckboxListTile(
          title: const Text('AppVesto'),
          subtitle: Text('Сompany of the future'),
          secondary: Icon(Icons.mobile_screen_share),
          activeColor: Colors.blue,
          value: _isChecked,
          onChanged: (bool value) {
            setState(() {
              _isChecked = value;
            });
          },
        ),
      ),
    );
  }
}
