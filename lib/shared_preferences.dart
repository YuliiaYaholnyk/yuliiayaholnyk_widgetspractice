import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:widgetsPractice/model/user_model.dart';

class SharedPreferencesPractice extends StatefulWidget {
  @override
  _SharedPreferencesPracticeState createState() => _SharedPreferencesPracticeState();
}

class _SharedPreferencesPracticeState extends State<SharedPreferencesPractice> {
  UserModel user = UserModel(
    name: 'Yuliia Yaholnyk',
    image:
        'https://media-exp1.licdn.com/dms/image/C4D03AQGNfPljImzDew/profile-displayphoto-shrink_200_200/0?e=1603929600&v=beta&t=CcfNYhCcSIhVCZH8aGGUVExqLTczO5fBL3K3cow3xNc',
    email: 'yuliiayaholnyk@appvesto.com',
    phone: '+380994161887',
  );

  @override
  void initState() {
    user.saveToPrefs().then((value) {
      user.getFromPrefs();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Package practice #1'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            SizedBox(height: 100.0,),
            ClipOval(
              child: Image(
                image: NetworkImage(user.image),
              ),
            ),
            Text(user.name),
            Text(user.phone),
            Text(user.email)
          ],
        ),
      ),
    );
  }
}
