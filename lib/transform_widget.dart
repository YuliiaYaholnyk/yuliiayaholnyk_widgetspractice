import 'dart:math';

import 'package:flutter/material.dart';

class TransformWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Padding'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child: Transform.rotate(
          angle: pi/4,
          child: IconButton(
            icon: Icon(Icons.android),
            onPressed: (){},
            iconSize: 60.0,
          ),
        ),
      ),
    );
  }
}
