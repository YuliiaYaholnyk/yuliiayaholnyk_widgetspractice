import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FittedBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('FittedBox'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Before'),
            Container(
              color: Colors.red,
              height: 100,
              width: 100,
              margin: EdgeInsets.all(30),
              child: Image(
                image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
              ),
            ),
            Text('After'),
            Container(
              color: Colors.red,
              height: 100,
              width: 100,
              margin: EdgeInsets.all(30),
              child: FittedBox(
                fit: BoxFit.fill,
                child: Image(
                  image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
