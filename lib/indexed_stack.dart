import 'package:flutter/material.dart';

class IndexedStackWidget extends StatefulWidget {
  @override
  _IndexedStackWidgetState createState() => _IndexedStackWidgetState();
}

class _IndexedStackWidgetState extends State<IndexedStackWidget> {
  int _widgetIndex = 0 ;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            child: IndexedStack(
              index: _widgetIndex,
              children: <Widget>[
                Semantics(
                  label: 'A gif',
                  child: Image(
                    image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Container(
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          maxWidth: 120.0
                        ),
                        child: RichText(
                          text: TextSpan(style: TextStyle(fontSize: 30.0, color: Colors.black), children: [
                            TextSpan(text: 'It`s  '),
                            TextSpan(text: '  all', style: TextStyle(fontWeight: FontWeight.bold)),
                            TextSpan(text: '  widgets', style: TextStyle(color: Colors.red)),
                          ]),
                        ),
                      )),
                ),
              ],
            ),
          ),
          RaisedButton(
            child: Text('Next index of stack'),
            onPressed: (){
              setState(() {
                _widgetIndex == 1? _widgetIndex = 0 :  _widgetIndex=1;
              });
            },
          )
        ],
      ),
    );
  }
}
