import 'package:flutter/material.dart';

class ColorFiltredWidget extends StatefulWidget {
  @override
  _ColorFiltredWidgetState createState() => _ColorFiltredWidgetState();
}

class _ColorFiltredWidgetState extends State<ColorFiltredWidget> {
  var filter = ColorFilter.mode(Colors.red, BlendMode.darken);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            child: ColorFiltered(
              colorFilter: filter,
              child: FadeInImage.assetNetwork(
                fadeInCurve: Curves.easeIn,
                fadeInDuration: const Duration(seconds: 2),
                placeholder: 'images/logo.png',
                image: 'https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704',
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              FlatButton(
                color: Colors.blue,
                onPressed: () {
                  setState(() {
                    filter = ColorFilter.mode(Colors.blue, BlendMode.modulate);
                  });
                },
              ),
              FlatButton(
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    filter = ColorFilter.mode(Colors.red, BlendMode.darken);
                  });
                },
              ),
              FlatButton(
                color: Colors.black87,
                onPressed: () {
                  setState(() {
                    filter =
                        ColorFilter.mode(Colors.black87, BlendMode.colorBurn);
                  });
                },
              ),
            ],
          )
        ],
      ),
    );
  }
}
