import 'dart:ui';

import 'package:flutter/material.dart';



class BackdropFilterWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Align'),),
      body: Stack(

        children: <Widget>[
          Container(
            color: Colors.red,
            height: 500,
            width: 400,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Image(
                height: 100,
                width: 100,
                image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
              ),
            ),
          ),
          Container(
            color: Colors.black.withOpacity(0.0),
          ),
        ],
      )
    );
  }
}
