import 'package:flutter/material.dart';


class ClipRRectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text('ClipRRect'),
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(20),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(15.0),
            clipBehavior: Clip.hardEdge,
            child: GestureDetector(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return ClipRRectPageDetail();
                }));
              },
              child: Hero(
                tag: 'tag',
                child: Image(
                  image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),

                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}



class ClipRRectPageDetail extends StatelessWidget {
  static const String routeName = 'detailPage';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      appBar: AppBar(
        title: Text('ClipRRect'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(20),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15.0),
              clipBehavior: Clip.hardEdge,
              child: Hero(
                tag: 'tag',
                child: Image(
                  image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: Text('You can create this animation in Flutter with Hero widgets. As the hero animates from the source to the destination route, the destination route (minus the hero) fades into view. Typically, heroes are small parts of the UI, like images, that both routes have in common. From the user’s perspective the hero “flies” between the routes. This guide shows how to create the following hero animations:Standard hero animationsA standard hero animation flies the hero from one route to a new route, usually landing at a different location and with a different size.'),
          ),
        ],
      ),
    );
  }
}
