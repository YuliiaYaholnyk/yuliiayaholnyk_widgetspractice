import 'package:flutter/material.dart';

class DataTableWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Table'),
      ),
      body: Center(
        child: DataTable(
          columns: [
            DataColumn(
              label: Text('Label'),
            ),
            DataColumn(
              label: Text('Label'),
            ),
            DataColumn(
              label: Text('Label'),
            ),

          ],
          rows: [
            DataRow(
              cells: [
                DataCell(Text('Data cell')),
                DataCell(Text('Data cell')),
                DataCell(Text('Data cell')),
              ]
            ),
            DataRow(
                cells: [
                  DataCell(Text('Data cell')),
                  DataCell(Text('Data cell')),
                  DataCell(Text('Data cell')),
                ]
            ),
          ],
        ),
      ),
    );
  }
}
