import 'package:flutter/material.dart';

class MyDragTarget extends StatefulWidget {
  @override
  _MyDragTargetState createState() => _MyDragTargetState();
}

class _MyDragTargetState extends State<MyDragTarget> {
bool isSuccessful = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body:  DragTarget(
        builder: (context, List<String> candidateData, rejectedData) {
          return Center(
            child: isSuccessful
                ? Padding(
              padding: EdgeInsets.only(top: 100.0),
              child: Container(
                color: Colors.yellow,
                height: 200.0,
                width: 200.0,
                child: Center(
                    child: Icon(Icons.favorite)),
              ),
            )
                : Container(
              height: 200.0,
              width: 200.0,
              color: Colors.yellow,
            ),
          );
        },
        onWillAccept: (data) {
          return true;
        },
        onAccept: (data) {
          setState(() {
            isSuccessful = true;
          });
        },
      ),
    );
  }
}
