import 'package:flutter/material.dart';

class RichTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RichText'),
      ),
      body: Container(
          child: RichText(
        text: TextSpan(style: TextStyle(fontSize: 30.0, color: Colors.black), children: [
          TextSpan(text: 'It`s  '),
          TextSpan(text: '  all', style: TextStyle(fontWeight: FontWeight.bold)),
          TextSpan(text: '  widgets', style: TextStyle(color: Colors.red)),
        ]),
      )),
    );
  }
}
