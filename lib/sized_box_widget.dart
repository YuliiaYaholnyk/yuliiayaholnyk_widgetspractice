import 'package:flutter/material.dart';


class SizedBoxWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sized Box')),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
               child: Image(
              image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
            ),
            ),
            SizedBox(
              height: 100,
              width: 100,
            ),
            Container(
              child: Image(
                image: NetworkImage('https://media1.tenor.com/images/12b3b5a0fd2ff64136509dea171b1df4/tenor.gif?itemid=11667704'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
