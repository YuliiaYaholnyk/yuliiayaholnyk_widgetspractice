import 'package:flutter/material.dart';
class CountModel extends InheritedModel<String> {

  final int count;

  CountModel({ this.count, child }) : super(child: child);

  @override
  bool updateShouldNotify(CountModel oldWidget) {
    if (oldWidget.count != count) {
      return true;
    }
    return false;
  }

  @override
  bool updateShouldNotifyDependent(InheritedModel<String> oldWidget, Set<String> dependencies) {
    if (dependencies.contains('counter')) {
      return true;
    }
    return false;
  }

  static CountModel of(BuildContext context, String aspect) {
    return InheritedModel.inheritFrom<CountModel>(context, aspect: aspect);
  }

}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Counter',
      theme: Theme.of(context),
      home: Counter(),
    );
  }
}

class Counter extends StatefulWidget {
  @override
  CounterState createState() => CounterState();
}

class CounterState extends State<Counter> {

  int count = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: Text("Counter"),
      ),
      body: CountModel(
          count: count,
          child: CounterText()
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            ++count;
          });
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

class CounterText extends StatelessWidget {
   CounterText();
  @override
  Widget build(BuildContext context) {
    CountModel model = CountModel.of(context, 'test');
    return Text('Count: ${model.count}');
  }

}