import 'package:flutter/material.dart';

class TweenAnimationBuilderWidget extends StatefulWidget {
  @override
  _TweenAnimationBuilderWidgetState createState() => _TweenAnimationBuilderWidgetState();
}

class _TweenAnimationBuilderWidgetState extends State<TweenAnimationBuilderWidget> {
  var value = 100.0;
  bool visible = false;
  bool positionAnim = false;
  double padValue = 0;

  @override
  void initState() {
    Future.delayed(Duration(seconds: 1), () {
      setState(() {
        visible = true;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          Center(
            child: Container(
              child: ClipOval(
                clipper: MyClipper(),
                child: TweenAnimationBuilder<double>(
                  duration: const Duration(seconds: 10),
                  tween: Tween<double>(begin: 0.0, end: value),
                  onEnd: () {
                    padValue = 100.0;
                  },
                  builder: (BuildContext ctx, double size, _) {
                    return Image(
                      width: size,
                      height: size,
                      image: NetworkImage('http://clipart-library.com/image_gallery2/Cartoon-PNG-Image.png'),
                      fit: BoxFit.cover,
                    );
                  },
                ),
              ),
            ),
          ),
          Container(
            child: AnimatedOpacity(
              duration: Duration(seconds: 5),
              onEnd: () {
                setState(() {
                  if (visible) {
                    visible = false;
                    return;
                  }
                  visible = true;
                });
              },
              opacity: visible ? 1.0 : 0.1,
              child: Image(
                height: 100.0,
                width: 100.0,
                image: NetworkImage('http://clipart-library.com/image_gallery2/Cartoon-PNG-Image.png'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          AnimatedPadding(
            padding: EdgeInsets.only(top: padValue),
            duration: Duration(seconds: 5),
            curve: Curves.fastLinearToSlowEaseIn,
            child: Image(
              height: 100.0,
              width: 100.0,
              image: NetworkImage('http://clipart-library.com/image_gallery2/Cartoon-PNG-Image.png'),
              fit: BoxFit.cover,
            ),
          ),
        ],
      ),
    );
  }
}

class MyClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, size.width, size.height);
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return false;
  }
}
