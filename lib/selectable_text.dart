import 'package:flutter/material.dart';

class SelectableTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select me'),
      ),
      body: Center(
        child: SelectableText(
          'Selectable text',
          style: TextStyle(color: Colors.blue),
          toolbarOptions: ToolbarOptions(copy: true, selectAll: true),
        ),
      ),
    );
  }
}
