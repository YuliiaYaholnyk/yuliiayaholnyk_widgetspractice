import 'package:flutter/material.dart';

class SpacerWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Divider'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 100.0,
            color: Colors.red,
          ),
          Divider(
            height: 10.0,
          ),
          Container(
            height: 100.0,
            color: Colors.blue,
          ),
          Divider(
            height: 10.0,
          ),
          Container(
            height: 100.0,
            color: Colors.green,
          ),
        ],
      ),
    );
  }
}
