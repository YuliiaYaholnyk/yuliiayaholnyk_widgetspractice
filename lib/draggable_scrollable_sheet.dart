import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DraggableScrollableSheetWidget extends StatefulWidget {
  @override
  _DraggableScrollableSheetWidgetState createState() => _DraggableScrollableSheetWidgetState();
}

class _DraggableScrollableSheetWidgetState extends State<DraggableScrollableSheetWidget> {
  List<String> list = List.generate(15, (index) => 'http://clipart-library.com/images/BcaKKLkni.png');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Draggable scrollable sheet'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            color: Colors.blueGrey,
            height: 100,
            width: double.infinity,
            child: Text(
              'Qua Qua',
              style: TextStyle(color: Colors.white, fontSize: 70, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            height: 400,
            color: Colors.blueGrey,
            child: DraggableScrollableSheet(
                initialChildSize: 0.5,
                maxChildSize: 1,
                minChildSize: 0.5,
                builder: (BuildContext ctx, controller) {
                  return ListView.builder(
                    controller: controller,
                    itemCount: list.length,
                    itemBuilder: (BuildContext ctx, int i) {
                      return Container(
                        margin: EdgeInsets.all(20),
                        decoration: BoxDecoration(border: Border.all(color: Colors.black87, width: 2.0, style: BorderStyle.solid)),
                        padding: EdgeInsets.all(15),
                        child: Image(
                          image: NetworkImage(list[i]),
                          fit: BoxFit.cover,
                        ),
                      );
                    },
                  );
                }),
          ),
        ],
      ),
    );
  }
}
