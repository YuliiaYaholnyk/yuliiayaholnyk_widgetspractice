import 'dart:ui';

import 'package:flutter/material.dart';

class StackWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('StackWidget'),
        ),
        body: Stack(
          children: <Widget>[
            Positioned(
              top: 2.0,
              left: 2.0,
              child: new Text(
                'Text with shadow',
                style: TextStyle(fontWeight: FontWeight.bold, color: Color(0x33000000), fontSize: 40.0),
              ),
            ),
            BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
              child: Text('Text with shadow', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.blue, fontSize: 40.0)),
            ),
          ],
        ));
  }
}
